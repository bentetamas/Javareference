/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyak11;

/**
 *
 * @author hallgato
 */
public class HianyosSor extends Exception
{

    /**
     * Creates a new instance of <code>HianyosSor</code> without detail message.
     */
    public HianyosSor()
    {
    }

    /**
     * Constructs an instance of <code>HianyosSor</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public HianyosSor(String msg)
    {
        super(msg);
    }
    
    public String toString(){
        return "Nincs elegendő adat a példányostáshoz!";
    }
}
