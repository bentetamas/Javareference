/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyak11;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import jdk.nashorn.internal.parser.Lexer;

/**
 *
 * @author hallgato
 */
public class Main
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        FileReader fr;
        Scanner sc;
        
        FileWriter fw;
        PrintWriter pw;
        
        String[] str;
        Orszag o = null;
        ArrayList<Orszag> lista = new ArrayList();
        
        try{
            fr = new FileReader("orszagok.csv");
            sc = new Scanner(fr);
            
            fw = new FileWriter("nepsurusegSzerint.txt");
            pw = new PrintWriter(fw);
            
            while(sc.hasNextLine()){
                try{
                    str = sc.nextLine().split(";");

                    if (str.length == 3){
                        o = new Orszag(str[0], Integer.parseInt(str[1].replace(" ", "")), Integer.parseInt(str[2]));
                    }
                    else{
                        throw new HianyosSor();
                    }

                    if(o!= null && !lista.contains(o)){
                        lista.add(o);
                    }
                    else{
                        System.out.println("Ez már szerepel a listában " + o.toString());
                    }
                }
                catch(HianyosSor e){
                    System.err.println(e.toString());
                }
            }
            
            Collections.sort(lista);
            
            for(Orszag i : lista){
                pw.println(i.toString());
            }
            
            fw.close();
            fr.close();
            
            Scanner sc2 = new Scanner(System.in);
            str = sc2.nextLine().split(" ");
            selectedOrszagok(Integer.parseInt(str[0]),Integer.parseInt(str[1]));
            
        }
        catch(FileNotFoundException e){
            System.err.println("A megadott fájl nem található!");
        }
        catch(IOException e){
            System.err.println("IO hiba!");
        }

        
    }
    
    
    public static void selectedOrszagok(int a, int b){
        FileReader fr;
        Scanner sc;
        String[] str;
        Orszag o;
        ArrayList<Orszag> lista = new ArrayList<>();
        
        try{
            fr = new FileReader("nepsurusegSzerint.txt");
            sc = new Scanner(fr);
            
            while(sc.hasNextLine()){
                str = sc.nextLine().split(", ");
                o = new Orszag(str[0], Integer.parseInt(str[1]), Integer.parseInt(str[2]));
                lista.add(o);
            }
            
            for(Orszag i : lista){
                if(i.getNepsuruseg()>a && i.getNepsuruseg()<b)
                    System.out.println(i.toString());
            }
            
            fr.close();
        }
        catch(FileNotFoundException e){
            System.err.println("A megadott fájl nem található!");
        }
        catch(IOException e){
            System.err.println("IO hiba!");
        }
        
        
    }
}
