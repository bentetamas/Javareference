/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gyak11;

/**
 *
 * @author hallgato
 */
public class Orszag implements Comparable
{
    private String nev;
    private int lakossag, terulet;
    
    public Orszag(String p1, int p2, int p3){
        nev = p1;
        lakossag = p2;
        terulet = p3;
    }

    public String getNev()
    {
        return nev;
    }

    public int getLakossag()
    {
        return lakossag;
    }

    public int getTerulet()
    {
        return terulet;
    }

    public void setNev(String nev)
    {
        this.nev = nev;
    }

    public void setLakossag(int lakossag)
    {
        this.lakossag = lakossag;
    }

    public void setTerulet(int terulet)
    {
        this.terulet = terulet;
    }
    
    public boolean equals(Object o){
        if (o==null)
            return false;
        
        if (this.getClass() != o.getClass())
            return false;
        
        Orszag s = (Orszag)o;
        
        if (this.getNev().compareTo(s.getNev())==0 && this.getLakossag() == s.getLakossag() && this.getTerulet() == s.getTerulet())
            return true;
        else
            return false;
        
    }
    
    public String toString(){
        return this.getNev() + ", " + this.getLakossag() + ", " + this.getTerulet() + ", (" + this.getNepsuruseg() +")";
    }
            
    public double getNepsuruseg(){
        return (double)this.getLakossag()/(double)this.getTerulet();
    }

    @Override
    public int compareTo(Object o)
    {
        Orszag s = (Orszag)o;
        if (this.getNepsuruseg()==s.getNepsuruseg()){
            return this.getNev().compareTo(s.getNev());
        }
        else{
            return (int)((s.getNepsuruseg()-this.getNepsuruseg())*1000);
        }
    }
}
